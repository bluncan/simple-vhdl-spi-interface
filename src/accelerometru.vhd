library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity accelerometru is
  Port (
        Clk: in STD_LOGIC;
        Rst: in STD_LOGIC;
        Read: in STD_LOGIC;
        MISO: in STD_LOGIC;
        MOSI: out STD_LOGIC;
        SCLK: out STD_LOGIC;
        SS: out STD_LOGIC;
        Data1: out STD_LOGIC_VECTOR(31 downto 0);
        Data2: out STD_LOGIC_VECTOR(31 downto 0)
   );
end accelerometru;

architecture Behavioral of accelerometru is
    signal TxData_i: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal RxData_i: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal TxRdy_i: STD_LOGIC := '0';
    signal RxRdy_i: STD_LOGIC := '0';
    signal Start_i: STD_LOGIC := '0';
    
    signal bytes_counter_i : INTEGER := 0;
    type stare is (INIT, SIGNAL_READ_REGISTER, SIGNAL_START, WAIT_TXRDY, SIGNAL_REGISTER, 
                     SIGNAL_START2, WAIT_RXRDY, WRITE_REGISTER, CHECK);
    signal state_i: stare := INIT;
    type register_array is array(0 to 7) of STD_LOGIC_VECTOR(7 downto 0);
    signal registers_i: register_array := (others => (others => '0'));
    signal register_enable_i: STD_LOGIC := '0';
begin
    spi_c: entity WORK.spi 
    generic map (WIDTH => 8)
    port map 
    (
        TxData => TxData_i,
        RxData => RxData_i,
        Clk => Clk,
        Rst => Rst,
        Start => Start_i,
        MISO => MISO,
        MOSI => MOSI,
        SCLK => SCLK,
        SS => SS,
        TxRdy => TxRdy_i,
        RxRdy => RxRdy_i
    );
    
    registers_c: process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                for i in 0 to 7 loop
                    registers_i(i) <= (others => '0');
                end loop;
            elsif (register_enable_i = '1') then
                registers_i(bytes_counter_i) <= RxData_i;
            end if;
        end if;
    end process;
    
    
    process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                state_i <= INIT;
            else
                case (state_i) is
                    when INIT =>
                        bytes_counter_i <= 0;
                        if (Read = '1') then
                            state_i <= SIGNAL_READ_REGISTER;
                        end if;
                    when SIGNAL_READ_REGISTER =>
                        state_i <= SIGNAL_START;
                    when SIGNAL_START =>
                        state_i <= WAIT_TXRDY;
                    when WAIT_TXRDY =>
                        if (TxRdy_i = '1') then
                            state_i <= SIGNAL_REGISTER;
                        end if;
                    when SIGNAL_REGISTER =>
                        state_i <= SIGNAL_START2;
                    when SIGNAL_START2 =>
                        state_i <= WAIT_RXRDY;
                    when WAIT_RXRDY =>
                        if (RxRdy_i = '1') then
                            state_i <= WRITE_REGISTER;
                        end if;
                    when WRITE_REGISTER =>
                        bytes_counter_i <= bytes_counter_i + 1;
                        state_i <= CHECK;
                    when CHECK =>
                        if (bytes_counter_i = 8) then
                            state_i <= INIT;
                        else
                            state_i <= WAIT_TXRDY;
                        end if; 
                end case;
            end if;
        end if;
    end process;
    
    with state_i select TxData_i <=
    X"0B" when SIGNAL_READ_REGISTER,
    X"0B" when SIGNAL_START,
    X"00" when others;
   
    Start_i <= '1' when (state_i = SIGNAL_START or state_i = SIGNAL_START2) else '0';
    register_enable_i <= '1' when (state_i = WRITE_REGISTER) else '0';
    
    Data1 <= registers_i(0) & registers_i(1) & registers_i(2) & registers_i(3);
    Data2 <= registers_i(4) & registers_i(5) & registers_i(6) & registers_i(7);
    
end Behavioral;
