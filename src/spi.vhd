library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity spi is
    generic (
            SCLK_FREQ: INTEGER := 5_000_000;
            WIDTH: INTEGER := 8
            );
    Port (
          TxData: in STD_LOGIC_VECTOR(WIDTH-1 downto 0);
          Start: in STD_LOGIC;
          Clk: in STD_LOGIC;
          Rst: in STD_LOGIC;
          
          MISO: in STD_LOGIC;
          MOSI: out STD_LOGIC;
          RxData: out STD_LOGIC_VECTOR(WIDTH-1 downto 0);
          SCLK: out STD_LOGIC;
          SS: out STD_LOGIC;
          TxRdy: out STD_LOGIC;
          RxRdy: out STD_LOGIC
     );
end spi;

architecture Behavioral of spi is
    constant CLK_FREQ: INTEGER := 100_000_000;
    constant FREQ_DIVIDER: INTEGER := CLK_FREQ / SCLK_FREQ;
    
    signal Start_int: STD_LOGIC := '0';
    signal RstStart: STD_LOGIC := '0';
    signal LdTxRx: STD_LOGIC := '0';
    signal ShTxRx: STD_LOGIC := '0';
    signal SCLK_int: STD_LOGIC := '0';
    signal SCLK_En: STD_LOGIC := '0';
    signal Q_RxReg: STD_LOGIC := '0';
    signal Q_TxDataReg: STD_LOGIC_VECTOR(WIDTH-1 downto 0) := (others => '0');
    signal Q_TxRxReg: STD_LOGIC_VECTOR(WIDTH-1 downto 0) := (others => '0');
    signal CE_n: STD_LOGIC := '0';
    signal CE_p: STD_LOGIC := '0';
    signal SCLK_i: STD_LOGIC := '0';
    signal Sout: STD_LOGIC := '0';
    signal SclkEn: STD_LOGIC := '0';
    
    type stare is (idle, load, tx_rx, bit0, ready);
    signal state: stare := idle; 
begin
    Gen_Sclk: process(Clk)
    variable count: INTEGER := 0;
    begin
         if (RISING_EDGE(Clk)) then
             if (Rst = '1') then
                count := 0;
                SCLK_i <= '0';
                CE_p <= '0';
                CE_n <= '0';
             else
                count := (count + 1) mod FREQ_DIVIDER;
                if ((count = FREQ_DIVIDER / 2 - 1) or (count = FREQ_DIVIDER - 1)) then
                    SCLK_i <= not SCLK_i;
                    if (SCLK_i = '0') then
                        CE_p <= '1';
                    elsif (SCLK_i = '1') then
                        CE_n <= '1';
                    end if;
                else
                    CE_n <= '0';
                    Ce_p <= '0';
                end if;
             end if;
         end if;
    end process Gen_Sclk;
    
    
    Rx_reg: process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                Q_RxReg <= '0';
             elsif (CE_p = '1') then
                Q_RxReg <= MISO;
             end if;
        end if;
    end process Rx_reg;
    
    Start_reg: process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (RstStart = '1') then
                Start_int <= '0';
            elsif (Start = '1') then
                Start_int <= '1';
            end if;
        end if;
    end process Start_reg;
    
    TxData_reg: process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                Q_TxDataReg <= (others => '0');
            elsif (Start = '1') then
                Q_TxDataReg <= TxData;
            end if;
         end if;
    end process TxData_reg;
    
    TxRx_reg: process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                Q_TxRxReg <= (others => '0');
            elsif (Ce_n = '1') then
                if (LdTxRx = '1') then
                    Q_TxRxReg <= Q_TxDataReg;
                elsif (ShTxRx = '1') then
                    Q_TxRxReg <= Q_TxRxReg(WIDTH-2 downto 0) & Q_RxReg;
                end if;
             end if;
        end if;
     end process TxRx_reg;
     Sout <= Q_TxRxReg(WIDTH-1);
     RxData <= Q_TxRxReg;
     MOSI <= Sout;
    
     
     control: process(Clk)
     variable CntBit: INTEGER := 0;
     begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                state <= idle;
            elsif (CE_n = '1') then
                case state is
                    when idle =>
                        if (Start_int = '1') then
                            state <= load;
                        end if;
                    when load =>
                        CntBit := WIDTH - 1;
                        state <= tx_rx;
                    when tx_rx =>
                        CntBit := CntBit - 1;
                        if (CntBit > 0) then
                            state <= tx_rx;
                         else
                            state <= bit0;
                         end if;
                    when bit0 =>
                        state <= ready;
                    when ready =>
                        CntBit := WIDTH - 1;
                        if (Start_int = '1') then
                            state <= tx_rx;
                        else
                            state <= idle;
                        end if;
                end case;
            end if;   
         end if;
     end process control;
     
     
     LdTxRx <= '1' when (state = load or (state = ready and Start_int = '1')) else '0';
     RstStart <= '1' when (state = load) else '0';
     ShTxRx <= '1' when (state = tx_rx or state = bit0) else '0';
     SclkEn <= '1' when (state = tx_rx or state = bit0) else '0';
   
     SS <= '1' when (state = idle) else '0';
     TxRdy <= '1' when (state = bit0) else '0';
     RxRdy <= '1' when (state = ready) else '0';
     SCLK <= SCLK_i when (SclkEn = '1') else '0';
end Behavioral;
