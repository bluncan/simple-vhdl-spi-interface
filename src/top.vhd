library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top is
Port (
        Clk: in STD_LOGIC;
        Rst: in STD_LOGIC;
        Read: in STD_LOGIC;
        MISO: in STD_LOGIC;
        MOSI: out STD_LOGIC;
        SCLK: out STD_LOGIC;
        SS: out STD_LOGIC;
        Sel: in STD_LOGIC;
        An: out STD_LOGIC_VECTOR(7 downto 0);
        Seg: out STD_LOGIC_VECTOR(7 downto 0)
   );
end top;

architecture Behavioral of top is
    signal display_data_i: STD_LOGIC_VECTOR(31 downto 0);
    signal read_debounced_i: STD_LOGIC;
    signal data1_i: STD_LOGIC_VECTOR(31 downto 0);
    signal data2_i: STD_LOGIC_VECTOR(31 downto 0);
    
begin
    displ7_segm_c: entity WORK.displ7_segm port map (
        Clk => Clk,
        Rst => Rst,
        Data => display_data_i,
        An => An,
        Seg => Seg
    );
    
    debounce_c: entity WORK.debounce port map (
        Clk => Clk,
        RSt => Rst,
        Din => Read,
        Qout => read_debounced_i
    );
    
    accelerometru_c: entity WORK.accelerometru port map (
        Clk => Clk,
        Rst => Rst,
        Read => read_debounced_i,
        MISO => MISO,
        MOSI => MOSI,
        SCLK => SCLK,
        SS => SS,
        Data1 => data1_i,
        Data2 => data2_i
    );
    
    process(Clk)
    begin
        if (RISING_EDGE(Clk)) then
            if (Rst = '1') then
                display_data_i <= (others => '0');
            elsif (Sel = '0') then
                display_data_i <= data1_i;
            else
                display_data_i <= data2_i;
            end if;
        end if;
    end process;

end Behavioral;
